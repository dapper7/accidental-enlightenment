﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class DialogueManager : MonoBehaviour {
    //public bool UseTypingEffect = true;
    private List<string> DialogueList;          //stores all of our dialogue for the game
    private List<char> LettersToBePrinted;
    public Text DialogueText;                   //the within "DialogueList" of all our dialogue

    public bool hasDisplayedText = false;
    public bool hasReachedEndOfText = false;

    private bool StopDialogueFlow = false;      //trigger to stop dialogue from continuing (stopping when we want it to)
    public List <string> LetterList;

    private int CurrentDialogueNumber = 0;

    private bool firstRun = false;

    public string textShownOnScreen;
    public float wordsPerSecond;

    private float timeElapsed = 0;
    //private string textt;

    public string PrintingText;
    public string CurrentText = "This is default text";
    public string TEXT = "printing";

    public GameObject GOPlayerControllerManagerScript;
    public PlayerControllerManager PlayerControllerManagerScript;
  
    void Start ()
    {
        GOPlayerControllerManagerScript = GameObject.Find("PlayerControllerManager");
        PlayerControllerManagerScript = GOPlayerControllerManagerScript.GetComponent<PlayerControllerManager>();
        DialogueList = new List<string>();
        LettersToBePrinted = new List<char>();
        LetterList = new List<string>();
       
	}

    void Update()
    {
        timeElapsed += Time.deltaTime;
       // Print();    
       // PrintingText = WaitThenPrint(TEXT, timeElapsed * wordsPerSecond);
    }

    public void StartDialouge(Dialogue dialogue)
    {
        CurrentText = "";
       // Dialogue CurrentTextBox;
        //clear the list before we begin
        DialogueList.Clear();

            //adds all the text boxes from other script into our queue
            foreach (string eachDialogue in dialogue.TextBoxes)
            {
            if (firstRun == false)
            {
                firstRun = true;
                foreach (char c in eachDialogue)
                {
                    string cString = c.ToString();
                    LetterList.Add(cString);
                    DialogueText.text = cString;
                }
            }
            else
            {
                LetterList.Add(" ");
                foreach (char c in eachDialogue)
                {
                    string cString = c.ToString();
                    LetterList.Add(cString);
                    DialogueText.text = cString;
                }
            }
       }
        for (int i = 0; i + 1 <= LetterList.Capacity; i++)
        {
            CurrentText += LetterList[i];
            
            //DialogueText.text = CurrentText;
        }
        //print
        PrintingText = WaitThenPrint(CurrentText, timeElapsed * wordsPerSecond);
        StopDialogueFlow = false;
    }

    public void Print()
    {
        PrintingText = WaitThenPrint(TEXT, timeElapsed * wordsPerSecond);
    }

    private string WaitThenPrint(string text, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < text.Length; i++)
        {
            words--;
            DialogueText.text = PrintingText;

            if (words <= 0)
            {
                return text.Substring(0, i);
            }
        }
        return text;
    }

    public string DisplayNextDialogueTypingEffect(string text, float wordCount)
    {
      
        if (DialogueList.Count == 0)
        {
            EndDialogue();
            return null;
        }
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < text.Length; i++)
        {
            words--;
            DialogueText.text = textShownOnScreen;

            if (words <= 0)
            {
                continue;
               // return text.Substring(0, i);
            }
        }
        return text;

        //display

    }

    //inputting the number from the for loop into this function to so it knows what dialogue to use
    public void DisplayNextDialogue()
    {
        if (DialogueList.Count == 0)
        {
            EndDialogue();
            return;
        }
            //the current dialogue in our list at the index(position) of whatever the for loop is at
            string CurrentDialogue = DialogueList[0];
            //print to console
            Debug.Log(DialogueList[CurrentDialogueNumber]);
        //converting the string into a text format to be used as a text for unityC
        //DialogueText.text = CurrentDialogue;
        // StartCoroutine(DisplayTextTypingEffect(CurrentDialogue, DialogueText));
        textShownOnScreen = PrintTextTypingEffect( CurrentDialogue, timeElapsed * wordsPerSecond);

        DialogueList.RemoveAt(0);
    }

    void EndDialogue()
    {
        //can add features for when the dialogue ends
        Debug.Log("End of Dialogue.");
        //setting the stop dialogue flowing trigger to true
        StopDialogueFlow = true;
        return;
    }

    private string PrintTextTypingEffect(string text, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < text.Length; i++)
        {
            words--;
            DialogueText.text = textShownOnScreen;

            if (words <= 0)
            {
                return text.Substring(0, i);
            }
        }
        return text;
    }


    IEnumerator DisplayTextTypingEffect(string dialogueText, Text textBox)
    {
        for (int i = 0; i < dialogueText.Length + 1; i++)
        {
            CurrentText = dialogueText.Substring(0, i);
            //this.GetComponent<Text>().text = CurrentText;
            textBox.text = dialogueText;


            Debug.Log("Length of dialogue " + dialogueText.Length);
            Debug.Log("How many times this loop goes for " + i);

            yield return new WaitForSecondsRealtime(wordsPerSecond);
        }
    }

    void DialogueBasedOnDirection()
    {
        if (PlayerControllerManagerScript.currentButton == "Left Button")
        {
           // StartDialouge()
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

[System.Serializable]
public class Dialogue 
{
    //how many lines we want the text boxes in the inspector to resize. 3 is min, 10 is max. (can go past max but scroll bar comes in when u go past max)
    [TextArea(3, 10)]
    // our text box where we write the dialogue
    public string[] TextBoxes;

    [TextArea(3, 10)]
    public string[] LeftTextBoxes;
}

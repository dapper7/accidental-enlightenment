﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AnimatorOffButton : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData data)
    {
        GetComponent<Animator>().SetBool("Conversating", false);

        DialogueManagerMain dialogueManager;

        try
        {
            dialogueManager = GameObject.Find("DialogueManager").GetComponent<DialogueManagerMain>();
        }
        catch (System.Exception)
        {
            throw new System.Exception("Scene must contain an object called \"DialogueManager\" with a DialogueManagerMain component attached ");
        }

        dialogueManager.currentAnimator = null;
        dialogueManager.setActivateDialoguePanel(false);

        GetComponent<Animator>().Play("Goodbye", 0);
    }
}

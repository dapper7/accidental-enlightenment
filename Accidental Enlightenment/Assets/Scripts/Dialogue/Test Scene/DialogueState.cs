﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueState : StateMachineBehaviour
{
    [SerializeField]
    string dialogueText;
    [SerializeField]
    string[] answers;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //turn every answer in the animator (dialouge controller) to false
        for (int i = 1; i < 5; i++)
        {
            string answerName = "Answer" + i;
            animator.SetBool(answerName, false);
        }

        DialogueManagerMain dialogueManager;
        try
        {
            dialogueManager = GameObject.Find("DialogueManager").GetComponent<DialogueManagerMain>();
        }
        catch (System.Exception)
        {
            throw new System.Exception("Scene must contain an object called \"DialogueManager\" with a DialogueManagerMain component attached");
        }

        dialogueManager.UpdateDialogueText(dialogueText);
        dialogueManager.GenerateAnswers(answers);
        dialogueManager.setActivateDialoguePanel(true);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        DialogueManagerMain dialogueManager;
        try
        {
            dialogueManager = GameObject.Find("DialogueManager").GetComponent<DialogueManagerMain>();
        }
        catch (System.Exception)
        {
            throw new System.Exception("Scene must contain an object called \"DialogueManager\" with a DialogueManagerMain component attached");
        }

        dialogueManager.RemoveAnswers();
    }

}

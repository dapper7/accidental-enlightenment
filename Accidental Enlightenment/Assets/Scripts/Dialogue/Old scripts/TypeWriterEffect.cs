﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TypeWriterEffect : MonoBehaviour
{

    public float TypingDelay;
    public string FullText;
    private string CurrentText;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(ShowText());
	}

    IEnumerator ShowText()
    {
        for (int i = 0; i < FullText.Length +1; i++)
        {
            CurrentText = FullText.Substring(0, i);
            this.GetComponent<Text>().text = CurrentText;
         
            yield return new WaitForSecondsRealtime(TypingDelay);
        }
    }

}

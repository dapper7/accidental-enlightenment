﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PupilBehaviour : MonoBehaviour {

    public GameObject eyesWhite;

    Vector2 mousePosition;

    Vector2 pupilPosition;
    float pupilX;
    float pupilY;

    Vector2 eyesWhitePosition;
    float eyesWhiteX;
    float eyesWhiteY;

	void Start ()
    {
        mousePosition = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        pupilPosition = gameObject.transform.position;
        pupilX = gameObject.transform.position.x;
        pupilY = gameObject.transform.position.y;

        eyesWhitePosition = eyesWhite.transform.position;
        eyesWhiteX = eyesWhite.transform.position.x;
        eyesWhiteY = eyesWhite.transform.position.y;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //StayWithinEyes();
        FollowMouse();
    }

    void StayWithinEyes()
    {
        if (pupilX > eyesWhiteX && pupilY > eyesWhiteY)
        {
            pupilPosition = eyesWhitePosition;
        }
    }

    void FollowMouse()
    {
        pupilPosition = mousePosition;
    }

   
}

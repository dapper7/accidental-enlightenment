﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryElement : MonoBehaviour {

    public Dialogue dialogue;

        
    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialouge(dialogue);
        
    }

    public void TriggerPrintTest()
    {
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvertoryManager : MonoBehaviour
{
    [SerializeField]
    Text[] invertoryObjectsTexts;

    [SerializeField]
    GameObject invertoryPanel;

    private bool isOpenPanel;

    public List<InvertoryObjects> InvertoryObjectsList = new List<InvertoryObjects>();

    private void Awake()
    {
        invertoryPanel.SetActive(false);
        isOpenPanel = false;
    }

    //debug
    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (!isOpenPanel)
            {
                UpdateInvertoryList();
                invertoryPanel.SetActive(true);
                isOpenPanel = true;
            }
         

            if (isOpenPanel)
            {
                invertoryPanel.SetActive(false);
                isOpenPanel = false;
                UpdateInvertoryList();
            }
        }
    }

    public void UpdateInvertoryList()
    {
        //convert the list to an array so its more in order as a list is a loosely collection of objects that may not index in the right order where an array does
        InvertoryObjects[] invertory = InvertoryObjectsList.ToArray();

        if (invertory.Length == 0)
        {
            for (int i = 0; i < invertoryObjectsTexts.Length; i++)
            {
                invertoryObjectsTexts[i].text = "Empty";
            }
        }

        else if (invertory.Length < invertoryObjectsTexts.Length)
        {
            for (int i = 0; i < invertory.Length; i++)
            {
                //use the text from InvertoryObjects.cs for the name 
                invertoryObjectsTexts[i].text = invertory[i].NameForList;
            }
        }
        else if (invertoryObjectsTexts.Length < invertory.Length)
        {
            for (int i = 0; i < invertoryObjectsTexts.Length; i++)
            {
                invertoryObjectsTexts[i].text = invertory[i].NameForList;
            }
        }
    }
}

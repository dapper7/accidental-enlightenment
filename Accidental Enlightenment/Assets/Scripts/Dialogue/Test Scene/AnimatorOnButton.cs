﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AnimatorOnButton : MonoBehaviour, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData data)
    {
        if (!GetComponent<Animator>().isActiveAndEnabled)
            {
                GetComponent<Animator>().enabled = true;

            GetComponent<Animator>().SetBool("isConversating", true);

            GetComponent<Animator>().Play("Hello", 0);

            DialogueManagerMain dialogueManager;

            try
            {
                dialogueManager = GameObject.Find("DialogueManager").GetComponent<DialogueManagerMain>();
            }
            catch (System.Exception)
            {
                throw new System.Exception("Scene must contain an object called \"DialogueManager\" with a DialogueManagerMain component attached ");
            }

            dialogueManager.currentAnimator = gameObject;

            dialogueManager.setActivateDialoguePanel(true);

        }
    }
	
}

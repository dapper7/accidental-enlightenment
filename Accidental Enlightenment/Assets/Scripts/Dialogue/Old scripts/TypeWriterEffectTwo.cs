﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeWriterEffectTwo : MonoBehaviour
{
    public Text textBox;

    public string textShownOnScreen;
    public string fullText = "The text you want shown on screen with typewriter effect.";
    public float wordsPerSecond = 2; // speed of typewriter
    private float timeElapsed = 0;

    void Update()
    {
        timeElapsed += Time.deltaTime;
        textShownOnScreen = GetWords(fullText, timeElapsed * wordsPerSecond);
    }

    private string GetWords(string text, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < text.Length; i++)
        {
            words--;
            textBox.text = textShownOnScreen;

            if (words <= 0)
            {
                return text.Substring(0, i);
            }
        }
        return text;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItemExchangeState : StateMachineBehaviour
{
    [SerializeField]
    bool isGiving;
    [SerializeField]
    InvertoryObjects ObjectForExchange;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        InvertoryManager invertoryManager;

        try
        {
            invertoryManager = GameObject.Find("InvertoryManager").GetComponent<InvertoryManager>();
        }
        catch(System.Exception)
        {
            throw new System.Exception("Scene must contain an object called \"InvertoryManager\" with an InvertoryManager attached");
        }

        if (isGiving && invertoryManager.InvertoryObjectsList.Contains(ObjectForExchange))
        {
            animator.SetBool("hasItem", true);
        }
        if (isGiving && !invertoryManager.InvertoryObjectsList.Contains(ObjectForExchange))
        {
            animator.SetBool("hasItem", false);
            invertoryManager.InvertoryObjectsList.Add(ObjectForExchange);
            invertoryManager.UpdateInvertoryList();
        }

        if (!isGiving && invertoryManager.InvertoryObjectsList.Contains(ObjectForExchange))
        {
            animator.SetBool("hasItem", true);
            invertoryManager.InvertoryObjectsList.Remove(ObjectForExchange);
            invertoryManager.UpdateInvertoryList();
        }

        if (!isGiving && !invertoryManager.InvertoryObjectsList.Contains(ObjectForExchange))
        {
            animator.SetBool("hasItem", false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerPanel : MonoBehaviour
{
    //keep everything private that is private but have it still show in the inspector
    [SerializeField]

    //answer buttons
    GameObject[] answerObjects;

    public void AddAnswers(string[] answers)
    {
        for (int i = 1; i < answers.Length; i++)
        {
            answerObjects[i].GetComponentInChildren<Text>().text = answers[i];
        }
    }

    public void RemoveAnswers()
    {
        for (int i = 0; i < answerObjects.Length; i++)
        {
            answerObjects[i].GetComponentInChildren<Text>().text = "";
        }
    }

    public void CatchAnswers(int id)
    {
        string answerName = "Answer" + id;
        GameObject.Find("DialogueManager").GetComponent<DialogueManagerMain>().currentAnimator.GetComponent<Animator>().SetBool(answerName, true);
    }

}

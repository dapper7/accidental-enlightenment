﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueCloseState : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        for (int i = 0; i < 5; i++)
        {
            string answerName = "Answer" + i;
            animator.SetBool(answerName, false);
        }

        DialogueManagerMain dialogueManager;
        try
        {
            dialogueManager = GameObject.Find("DialogueManager").GetComponent<DialogueManagerMain>();
        }
        catch (System.Exception)
        {
            throw new System.Exception("Scene must contain an object called \"DialogueManager\" with a DialogueManagerMain component attached");
        }

        dialogueManager.RemoveAnswers();
        dialogueManager.setActivateDialoguePanel(false);
        dialogueManager.currentAnimator = null;

        animator.enabled = false;
    }

         
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManagerMain : MonoBehaviour
{
    //keep everything private that is private but have it still show in the inspector
    [SerializeField]
    GameObject dialoguePanel;
    [SerializeField]
    AnswerPanel answerPanel;

    //store current dialogue controller in the Animator variable (so if we ever want to add more than one dialogue controllers we can)
    //we also dont want to see currentAnimator in the inspector
    [HideInInspector]
    public GameObject currentAnimator;

    //set the current dialoguePanel true or false anywhere in our code
    public void setActivateDialoguePanel(bool b)
    {
        dialoguePanel.SetActive(b);
    }

    public void GenerateAnswers(string[] a)
    {
        answerPanel.AddAnswers(answers: a);
    }

    public void UpdateDialogueText(string s)
    {
        dialoguePanel.GetComponentInChildren<Text>().text = s;
    }

    public void RemoveAnswers()
    {
        answerPanel.RemoveAnswers();
    }
}

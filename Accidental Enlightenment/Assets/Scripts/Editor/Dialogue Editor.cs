﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class DialogueEditorWindow : EditorWindow
{
    Rect window;
    //Rect window2;
    //Rect window3;

 
    private List<Rect> NodeList = new List<Rect>();

    [MenuItem("Tools/Dialogue Editor")]
    public static void ShowWindow()
    {
        DialogueEditorWindow editor = EditorWindow.GetWindow<DialogueEditorWindow>();
        editor.Init();
    }
    public void Init()
    {
        for (int i = 0; i + 1 <= NodeList.Capacity; i++)
        {
            NodeList[i] = new Rect(100, 100, 100, 100);
        }

        //foreach (Rect rectWindow in NodeList)
        //{
        //    rectWindow = new Rect(10, 10, 100, 100);
        //}
    }

    void OnGUI()
    {
        // DrawNodeCurve(window1, window2); // Here the curve is drawn under the windows
        BeginWindows();
        if (GUILayout.Button("Create Dialogue"))
        {
            window = GUI.Window(1, window, DrawNodeWindow, "Window 1");
            NodeList.Add(window);

            //if (NodeList.Capacity >= 2)
            //{
            //    DrawNodeCurve(NodeList[0], NodeList[NodeList.Capacity]);
            //}
        }

        
        //window1 = GUI.Window(1, window1, DrawNodeWindow, "Window 1");   // Updates the Rect's when these are dragged
        //window2 = GUI.Window(2, window2, DrawNodeWindow, "Window 2");
        //window3 = GUI.Window(3, window3, DrawNodeWindow, "Window 3");
        EndWindows();
    }

    void DrawNodeWindow(int id)
    {
        GUI.DragWindow();
    }

    void DrawNodeCurve(Rect start, Rect end)
    {
       //Vector3 startPos = Vector3.zero;
        Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
        Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
        Vector3 startTan = startPos + Vector3.right * 50;
        Vector3 endTan = endPos + Vector3.left * 50;
        Color shadowCol = new Color(0, 0, 0, 0.06f);
        for (int i = 0; i < 3; i++) // Draw a shadow
            Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);
        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
    }

}
